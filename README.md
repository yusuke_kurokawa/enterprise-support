# README #

The collection of the Enterprise Support Scripts & Examples are open source and available for use in your games and applications. This document details how to get it up and running on your computer and start modifying the code!

### What license are the Enterprise Support Scripts & Examples are shipped under? ###
The Unity Enterprise Support Scripts & Examples are released under an MIT license; see the LICENSE file.

This means that you pretty much can customize and embed it in any software under any license without any other constraints than preserving the copyright and license information while adding your own copyright and license information.

You can keep the source to yourself or share your customized version under the same MIT license or a compatible license.

If you want to contribute patches back, please keep it under the unmodified MIT license so it can be integrated in future versions and shared under the same license. Also make sure to follow the Unity coding standard. 

### How do I get started? ###
* (optional) Fork this repository into your own BitBucket account.
* Clone this repository onto a location on your computer.
* Configure your IDE for the Unity coding standard, look in the .editorconfig file for more information.
* Open a project in Unity. Most examples exit as a standard Unity project folder, each example has separate instuctions on how to use it. 
* Modify the code as you see fit.

### Will you be taking pull requests? ###
We will be developing the Enterprise Support Scripts & Examples directly in the public repository, we will happily take pull requests if they improve any of the scripts or examples we currently have available.

If you want to contribute to the repository please do the following:

* Fork and clone the Unity repository: [Doc](https://confluence.atlassian.com/bitbucket/fork-a-teammate-s-repository-774243391.html)
* In your fork create a new branch based off trunk
* Make the changes you wish
* Make a pull request from your branch to the same one on the Unity repo (you may have to allow the creation of the new branch): [Doc](https://confluence.atlassian.com/bitbucket/create-a-pull-request-774243413.html)
* We will accept or reject it.
* We will run some internal code beautification tools
* We will close the branch and merge it to trunk


### C# Conventions ###

Configure your IDE for the Unity coding standard, look in the .editorconfig file for more information or apply the Enterprise-Support-Style-Policy.mdpolicy to your MonoDevelop IDE.

* CamelCase Function 
* No ForEach
* No g_, s_, m_, k_
* No var
* No spaces before parens

```csharp
NameSpace Name
{
	Class NameClass
	{

		private int _privateVariable
		public float PublicVariable

		if (X) 
		{
			...
		}

		for (I = 0; I != 100; ++I) 
		{
			...
		}

		while (LLVMRocks) {
			...
		}

		SomeFunction(42);
		assert(3 != 4 && "laws of math are failing me");
		 
		A = Foo(42, 92) + Bar(X);

	}
}
```
