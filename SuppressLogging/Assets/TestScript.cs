﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{
	public Text onScreenText;

	void Update ()
	{
		Debug.Log ("Log Message");
	}

	void OnEnable ()
	{
		Application.RegisterLogCallback (HandleLog);
	}

	void OnDisable ()
	{
		Application.RegisterLogCallback (null);
	}

	void HandleLog (string message, string stackTrace, LogType type)
	{
		onScreenText.text = message;
	}
}
