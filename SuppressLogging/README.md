# README #

The Suppress Logging example shows how to effectively suppress all logging commands using the Debug namespace. This is usefull when you want to turn off logging to perform some debugging tasks. It does not relpace a proper logger which can be based on [ConditionalAttribute](https://msdn.microsoft.com/en-us/library/4xssyw96(v=vs.90).aspx) to work on the preprocesser level. 

### How to use ###

* Open the Main scene and press play.
* You will see log output from the TestScript. 
* Comment out #define LOGGING_CONDITION to disables logging. 
* You will see there is no logoutput anymore. 

### How it works ###
The static class Debug overrides the Debug namespace and makes it possible to reroute the Log messages. 

The [ConditionalAttribute](https://msdn.microsoft.com/en-us/library/4xssyw96(v=vs.90).aspx) basically removes the function call and the function itself if the expression is not met (LOGGING_CONDITION not defined), therefore you do not have the costs of the actual call anymore, which can be used in the a logger as well to make it more effecient. 

However, you still can log messages if needed by calling the actual engine namespace via UnityEngine.Debug.